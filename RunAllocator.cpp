// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>

#include "Allocator.hpp"

using namespace std;
using atype = double;
const int asize = 1000;


// ---------------
// print_allocator
// ---------------
/**
 * Prints all the values of an allocator by iterating through it
 * @param a const reference to an allocator
 * @param an ostream to print to
 */
void print_allocator(const my_allocator<atype, asize>& x, std::ostream& sout) {

    my_allocator<atype, asize>::const_iterator b = x.begin();
    my_allocator<atype, asize>::const_iterator e = x.end();
    while(b != e) {
        if(b == x.begin()) {
            sout << *b;
        }
        else {
            sout << " " << *b;
        }
        ++b;
    }
    sout << "\n";

}

// ----
// main
// ----
int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    //Read in the number of tests into t
    string s;
    getline(cin, s);
    int t;
    t = stoi(s);
    assert(t > 0 && t <= 100);

    getline(cin, s);
    while(t > 0) {
        t--;
        getline(cin, s);

        //Read in all the int instructions into a vector
        vector<int> instructions;
        while(s.length() > 0) {
            instructions.push_back(stoi(s));
            getline(cin, s);
        }

        my_allocator<atype, asize> x;
        //Evaluate all instructions
        for(int i : instructions) {
            //Deallocate
            if(i < 0) {
                //Find the ith allocated block
                my_allocator<atype, asize>::iterator b = x.begin();
                while(i < 0) {
                    if(*b < 0) {
                        ++i;
                    }
                    if(i != 0) {
                        ++b;
                    }
                }
                atype *p = reinterpret_cast<atype*>((&*b)+1);
                x.deallocate(p, 0);
            }
            //Allocate
            else {
                x.allocate(i);
            }
        }
        print_allocator(x, cout);
    }
    return 0;
}

