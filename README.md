# CS371p: Object-Oriented Programming Allocator Repo

* Name: (your Full Name) Daniel Young

* EID: (your EID) day397

* GitLab ID: (your GitLab ID) danyoungday

* HackerRank ID: (your HackerRank ID) danyoung

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok) 9aeb4adc349f6e65f8c2bed11b1cca0e519fa456

* GitLab Pipelines: (link to your GitLab CI Pipeline) https://gitlab.com/danyoungday/cs371p-allocator/-/pipelines/204258789

* Estimated completion time: (estimated time in hours, int or float) 10

* Actual completion time: (actual time in hours, int or float) 12
10/16: 2
10/17: 5
10/18: 5

* Comments: (any additional comments you have) Used Branden James' ctd from piazza.
