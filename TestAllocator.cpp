// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test #fixed

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test #fixed


// -----------
// Constructor
// -----------
//Test if the constructor properly sets b
TEST(AllocatorFixture3, constructor0) {
    my_allocator<double, 1000> x;
    my_allocator<double, 1000>::iterator b = x.begin();
    ASSERT_EQ(*b, 992);
}

// --------
// Allocate
// --------
//Test if allocating and filling up the heap works properly
TEST(AllocatorFixture3, allocate0) {
    my_allocator<double, 1000> x;
    for(int i = 0; i < 5; i++) {
        x.allocate(24);
    }

    my_allocator<double, 1000>::iterator b = x.begin();
    my_allocator<double, 1000>::iterator e = x.end();
    while(b != e) {
        ASSERT_EQ(*b, -192);
        ++b;
    }
}

// ----------
// Deallocate
// ----------
//Test if a manually allocated heap can be deallocated
TEST(AllocateFixture3, deallocate0) {
    my_allocator<double, 1000> x;
    my_allocator<double, 1000>::iterator b = x.begin();
    int* e = reinterpret_cast<int*>(reinterpret_cast<char*>(&*b) + *b + sizeof(int));
    *b = -1 * *b;
    *e = *b;
    ASSERT_EQ(*b, -992);
    ASSERT_EQ(*e, -992);
    x.deallocate(reinterpret_cast<double*>(&*b + 1), 0);
    ASSERT_EQ(*b, 992);
    ASSERT_EQ(*e, 992);
}

//Test coalesce from both sides
TEST(AllocateFixture3, deallocate1) {
    my_allocator<double, 1000> x;
    x.allocate(24);
    x.allocate(24);
    my_allocator<double, 1000>::iterator b = x.begin();
    ASSERT_EQ(*b, -192);
    x.deallocate(reinterpret_cast<double*>(&*b + 1), 0);
    ASSERT_EQ(*b, 192);
    ++b;
    x.deallocate(reinterpret_cast<double*>(&*b + 1), 0);
    b = x.begin();
    ASSERT_EQ(*b, 992);
}

// --------
// Iterator
// --------
//Tests equality and addition
TEST(AllocateFixture3, iterator0) {
    my_allocator<double, 1000> x;
    my_allocator<double, 1000>::iterator b = x.begin();
    my_allocator<double, 1000>::iterator e = x.end();
    my_allocator<double, 1000>::iterator p = b;
    for(int i = 0; i < 5; i++) {
        x.allocate(24);
        ASSERT_EQ(*(p++), -192);
    }
    ++++++++++b;
    ASSERT_EQ(b == e, true);
}

//Test * modifying capabilities
TEST(AllocateFixture3, iterator1) {
    my_allocator<double, 1000> x;
    my_allocator<double, 1000>::iterator b = x.begin();
    ASSERT_EQ(*b, 992);
    *b = 5;
    ASSERT_EQ(*b, 5);
}
