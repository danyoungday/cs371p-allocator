// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------
        /**
        * Compares 2 iterators by the address of their contained pointers
        * @param an iterator
        * @param an iterator
        * @return whether or not the 2 iterators contain pointers that point to the same address in memory
        */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>

            return lhs._p == rhs._p;
        }                                           // replace! #replaced

        // -----------
        // operator !=
        // -----------
        /**
        * Compares 2 iterators by the address of their contained pointers
        * @param an iterator
        * @param an iterator
        * @return whether or not the 2 iterators contain pointers that point to different addresses in memory
        */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        //_p points to the address in memory of the beginning sentinel to a block of memory
        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------
        /**
        * Dereferences our iterator
        * @return a reference to the value pointed to by _p (a sentinel)
        */
        int& operator * () const {
            // <your code>
            return *_p;
        }           // replace! #replaced

        // -----------
        // operator ++
        // -----------
        /**
        * Increments our iterator by 1 by adding however many bytes it points to + the size of an int sentinel
        * @return the iterator after it has been incremented
        */
        iterator& operator ++ () {
            // <your code>
            int size = abs(*_p);
            //Convert to bytes, add size of object + sentinels
            char* ptr = reinterpret_cast<char*>(_p);
            ptr += size + 2 * sizeof(int);
            //Set _p to new pointer
            _p = reinterpret_cast<int*>(ptr);
            return *this;
        }

        // -----------
        // operator ++
        // -----------
        /**
        * Increments our iterator by 1 by adding however many bytes it points to + the size of an int sentinel
        * @return the iterator before it has been incremented
        */
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        /**
         * Decrements our iterator by decrementing our pointer by the size of an int, then however much the previous block contains, then the size of an int sentinel
         * @return a reference to the iterator after it has been decremented
         */
        iterator& operator -- () {
            // <your code>
            //Find how big the thing before us is
            --_p;
            int size = abs(*_p);
            //Convert to bytes, subtract size of object + beginning sentinel
            char* ptr = reinterpret_cast<char*>(_p);
            ptr -= (size + sizeof(int));
            //Set _p to the new pointer
            _p = reinterpret_cast<int*>(ptr);

            return *this;
        }

        // -----------
        // operator --
        // -----------
        /**
         * Decrements our iterator by decrementing our pointer by the size of an int, then however much the previous block contains, then the size of an int sentinel
         * @return the iterator before it has been decremented
         */
        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------
        /**
        * Compares 2 iterators by the address of their contained pointers
        * @param a const iterator
        * @param a const iterator
        * @return whether or not the 2 iterators contain pointers that point to the same address in memory
        */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }                                                       // replace! #replaced

        // -----------
        // operator !=
        // -----------
        /**
        * Compares 2 iterators by the address of their contained pointers
        * @param a const iterator
        * @param a const iterator
        * @return whether or not the 2 iterators contain pointers that point to different addresses in memory
        */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------
        /**
        * Dereferences our iterator
        * @return a const reference to the value pointed to by _p (a sentinel)
        */
        const int& operator * () const {
            // <your code>
            return *_p;
        }                 // replace! #replaced

        // -----------
        // operator ++
        // -----------
        /**
        * Increments our const iterator by 1 by adding however many bytes it points to + the size of an int sentinel
        * @return a reference to the const iterator after it has been incremented
        */
        const_iterator& operator ++ () {
            // <your code>
            const int size = abs(*_p);
            //Convert to bytes, add size of object + 2 ending sentinels
            const char* ptr = reinterpret_cast<const char*>(_p);
            ptr += size + 2 * sizeof(int);
            //Set _p to new pointer
            _p = reinterpret_cast<const int*>(ptr);
            return *this;
        }

        // -----------
        // operator ++
        // -----------
        /**
        * Increments our const iterator by 1 by adding however many bytes it points to + the size of an int sentinel
        * @return the const iterator before it has been incremented
        */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        /**
         * Decrements our const iterator by decrementing our pointer by the size of an int, then however much the previous block contains, then the size of an int sentinel
         * @return a reference to the const iterator after it has been decremented
         */
        const_iterator& operator -- () {
            // <your code>
            //Find how big the thing before us is
            --_p;
            const int size = abs(*_p);
            //Convert to bytes, subtract size of object + beginning sentinel
            char* ptr = reinterpret_cast<char*>(_p);
            ptr -= (size + sizeof(int));
            //Set _p to the new pointer
            _p = reinterpret_cast<int*>(ptr);
            return *this;
        }

        // -----------
        // operator --
        // -----------
        /**
         * Decrements our const iterator by decrementing our pointer by the size of an int, then however much the previous block contains, then the size of an int sentinel
         * @return the const iterator before it has been decremented
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     * Checks if our allocator is valid by doing 3 things:
     *      checks every set of sentinels and verifies that the beginning and end are equal
     *      checks the difference between each sentinel is sizeof(int) + value of sentinel
     *      checks the allocator totals to the size instantiated
     * @return whether or not the allocator is valid
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        int total = N;
        const_iterator b = begin();
        const_iterator e = end();
        while(b != e) {
            int size = abs(*b);
            //Decrement total to see if we sum to N
            total -= size + 2 * sizeof(int);
            //Points to the beginning int sentinel
            const int* bptr = &*b;
            //Converts b to char*, adds whatever is stored in b and size of int sentinel to get last int sentinel
            const int* eptr = reinterpret_cast<const int*>(reinterpret_cast<const char*>(bptr) + size + sizeof(int));

            //Check if the number of bytes between the 2 sentinels is correct
            if(reinterpret_cast<const char*>(eptr) - reinterpret_cast<const char*>(bptr) != size + sizeof(int)) {
                return false;
            }

            //Check if beginning and end sentinels are the same
            if(*bptr != *eptr) {
                return false;
            }

            ++b;
        }
        //If we have the right # of stuff return true
        return !total;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // <your code>
        if(N < sizeof(T) + (2 * sizeof(int))) {
            std::bad_alloc e;
            throw e;
        }
        int* b = reinterpret_cast<int*>(&(*this)[0]);
        *b = N - 2 * sizeof(int);
        //End sentinel goes after 1 int and the empty space
        int* e = reinterpret_cast<int*>(&(*this)[N-sizeof(int)]);
        *e = N - 2 * sizeof(int);
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param the number of T's to allocate
     * @return a T pointer to the beginning of the block of allocated memory
     */
    pointer allocate (size_type s) {
        // <your code>
        //If we are allocating too little or too much throw bad_alloc
        if(s < 1 || N < s * sizeof(T) + 2 * sizeof(int)) {
            std::bad_alloc ex;
            throw ex;
        }

        iterator b = begin();
        iterator e = end();

        int sbytes = s * sizeof(T);

        //need greater than or equal to exact room
        while(b != e && *b < sbytes) {
            ++b;
        }
        //If we can't find room throw bad_alloc
        if(b == e) {
            std::bad_alloc ex;
            throw ex;
        }
        //If there's not enough room for a free block just set the whole chunk to be allocated
        if(*b < sbytes + sizeof(T) + 2 * sizeof(int)) {
            //Convert to bytes, add size stored (*b positive)
            int* eptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&*b) + sizeof(int) + *b);

            //Just update b's sentinels and max it out
            *b = -1 * (*b);

            *eptr = *b;
        }
        else {
            //Amount to be stored in new empty block
            int diff = *b - (sbytes + 2 * sizeof(int));
            *b = -1 * (sbytes);
            int* p = &*b;
            p = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + sizeof(int) + sbytes);
            *p = *b;
            ++p;
            *p = diff;
            p = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + sizeof(int) + diff);
            *p = diff;
        }

        assert(valid());
        //Jump forward 1 int to get to free block
        pointer out = reinterpret_cast<pointer>((&*b) + 1);
        return out;
    }             // replace! #replaced

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     * Constructs a T at p with value v
     * @param a pointer
     * @param a const reference to a T
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     * Finds if we have to coalesce and sets the boundaries of the new block based on whether or not we coalesce.
     * Then sets these new sentinel values to the combined values of our old blocks and the pointers we no longer need.
     * Has undefined behavior when trying to access deallocated memory now.
     * @param a pointer to the start of the block to deallocate
     * @param a size_type that's necessary to be a subclass of allocator
     */
    void deallocate (pointer p, size_type) {
        // <your code>

        iterator b = begin();
        iterator e = end();

        //Check if p is in the bounds of the allocator
        if(reinterpret_cast<pointer>((&*b) + 1) > p || reinterpret_cast<pointer>((&*e) - 1) < p) {
            std::invalid_argument ex("p not within bounds of allocator");
            throw ex;
        }

        //Go back to sentinel instead of block
        int* bptr = reinterpret_cast<int*>(p) - 1;

        //Add what was in the sentinel to p to get the end sentinel
        int* eptr = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + abs(*bptr));

        //Validate bptr and eptr are sentinels making p a valid pointer to the start of a block of memory
        if(*bptr != *eptr || reinterpret_cast<char*>(eptr) - reinterpret_cast<char*>(bptr) != abs(*bptr) + sizeof(int)) {
            std::invalid_argument ex("p not a valid pointer to block of memory");
            throw ex;
        }

        bool back = false;
        bool forward = false;
        int* backptr = bptr;
        int* forwardptr = eptr;
        //if this isn't the first block and the block before us isn't allocated
        if(bptr > &*b && *(&*bptr - 1) > 0) {
            back = true;
            //Go back from end sentinel of previous block by value of sent + the size of an int to get start of beginning sent
            backptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&*bptr - 1) - *(&*bptr - 1) - sizeof(int));
        }
        //if this isn't the last block and the block after us isn't allocated
        if(eptr + 1 < &*e && *(&*eptr + 1) > 0) {
            forward = true;
            //Go forward from beginning sent size of int + value of beginning sent
            forwardptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&*eptr + 1) + sizeof(int) + *(&*eptr + 1));
        }

        int newval;
        //Coalesce the 3 blocks and take in the 4 sentinels we no longer need
        if(back && forward)
            newval = *backptr + abs(*bptr) + *forwardptr + 4 * sizeof(int);

        //Take last sentinel of block after us and first sentinel of us
        else if(forward)
            newval = abs(*bptr) + *forwardptr + 2 * sizeof(int);

        //Take first sentinel of block before us and last sentinel of us
        else if(back)
            newval = *backptr + abs(*bptr) + 2 * sizeof(int);

        //Take our first and last sentinel
        else
            newval = -1 * *bptr;

        //Update with new value
        *backptr = newval;
        *forwardptr = newval;

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     * Destroys the T at p
     * @param a pointer
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * Gets the ith byte of a as a reference to an int
     * @param an int index of a
     * @return an int reference to the ith byte of a
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * Gets the ith byte of a as a const reference to an int
     * @param an int index of a
     * @return a const int reference to the ith byte of a
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     * Creates an iterator pointing to the first sentinel of a
     * @return an iterator pointing to the first sentinel of a
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * Creates a const iterator pointing to the first sentinel of a
     * @return a const iterator pointing to the first sentinel of a
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     * Creates an iterator pointing to the end of a
     * @return an iterator pointing to the end of a
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * Creates a const iterator pointing to the end of a
     * @return a const iterator pointing to the end of a
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};



#endif // Allocator_h
